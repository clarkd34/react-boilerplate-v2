# Boilerplate React Application

## Prerequisites
- A Firebase database
- A Google account

## Getting Started
You'll need to place your Firebase database connection properties into a file 
called `.env.development`. Place this file in the root directory. This will be
picked up at runtime.

Then, run the following to get the webpack dev-server started:
```shell
$ yarn install
$ yarn run dev-server
```

## Testing
Jest is pre-configured with this project. The test config expects an environment 
file also. Place a file called `.env.test` into the root of the project. If 
you're going to test against a Firebase database, then provide your connection 
properties in this file too.

Then, just run:
```shell
# Run once
$ yarn test

# Run on file changes
$ yarn test --watch 
```